import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './CameraPreview.css';

import { CameraPreview, CameraPreviewOptions } from '@capacitor-community/camera-preview';
import { useState } from 'react';

interface TabProp {
  title?: string,
}

const TabCamera: React.FC<TabProp> = ({ title }) => {
  const [base64, setBase64] = useState("")
  const [capturing, setCaptureing] = useState(false)

  async function captureSample() {
    const result = await CameraPreview.captureSample({ quality: 85 });
    setBase64("data:image/png;base64, " + result.value);
  }

  function startPreview(position: string) {
    setCaptureing(true)
    CameraPreview.start(
      {
        parent: 'content',
        position: position,
        // height: 400,
        // width: 400,
        toBack: true,
        disableAudio: true,
        enableZoom: true,
      }
    );
  }

  function stopPreview() {
    CameraPreview.stop()
    setCaptureing(false)
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{title!}</IonTitle>
        </IonToolbar>
      </IonHeader>

      {/* ion-content has to be transparent to show the preview camera at back */}
      <IonContent class="content-camera-preview" fullscreen>
        <div className='row m-2'>
          <button type="button" className='col btn btn-primary m-1 zindex-dropdown' onClick={() => startPreview('front')} disabled={capturing}>Front</button>
          <button type="button" className='col btn btn-primary m-1 zindex-dropdown' onClick={() => startPreview('rear')} disabled={capturing}>Rear</button>
          <button type="button" className='col btn btn-primary m-1 zindex-dropdown' onClick={() => CameraPreview.flip()} disabled={!capturing}>Flip</button>
          <button type="button" className='col btn btn-primary m-1 zindex-dropdown' onClick={stopPreview} disabled={!capturing}>Stop</button>
          <button type="button" className='col btn btn-primary m-1 zindex-dropdown' onClick={captureSample} disabled={!capturing}>Capture</button>
        </div>

        <div className='input-group'>
          <textarea className='form-control' defaultValue={base64} />
        </div>
        <div className='d-flex justify-content-center m-2'>
          <div id="content" hidden={!capturing} />
          <img src={base64} hidden={capturing} />
        </div>
      </IonContent>
    </IonPage>
  );
};

export default TabCamera;
