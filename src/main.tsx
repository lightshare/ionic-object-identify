import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import 'bootstrap-icons/font/bootstrap-icons.css'

/* Capacitor plugins */
import '@capacitor-community/camera-preview'

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);