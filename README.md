## Created with Ionic and Capacitor CLI
```sh
# globally install cli
npm install -g @ionic/cli
# create an ionic project
ionic start

# add dependency for Capacitor
pnpm i @capacitor/core
# add devDependency for Capacitor-cli
pnpm i -D @capacitor/cli

# initialize Capacitor config
npx cap init
# add/update ios & android target platforms for Capacitor
pnpm i @capacitor/core @capacitor/ios @capacitor/android

# create platform native project for ios & android
# (optional)
npx cap add android
npx cap add ios
```

### Download Dependency
Recommend using `pnpm` as package manager.
```sh
pnpm install
```

### Start Serve
```sh
npm start
```

### Build Project
```sh
# vite build html
npm build
# Sync your web code to your native project​, install/update the required native dependencies
npx cap sync

# open simulator
npx cap run ios
# build
npx cap build ios
# open native IDE
npx cap open ios
```

## ios Resources Access
[Configuring Info.plist](https://capacitorjs.com/docs/ios/configuration)
[Build Plugin for ios](https://capacitorjs.com/docs/ios/custom-code)

## android Resources Access
[Configuring AndroidManifest.xml](https://capacitorjs.com/docs/android/configuration)
[Build Plugin for android](https://capacitorjs.com/docs/android/custom-code)
